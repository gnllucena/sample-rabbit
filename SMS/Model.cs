﻿
using System.IO;
using System.Xml;
using System.Xml.Serialization;
/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class Model
{
    private string identificadorField;

    private string celularField;

    private string mensagemField;

    /// <remarks/>
    public string identificador
    {
        get
        {
            return this.identificadorField;
        }
        set
        {
            this.identificadorField = value;
        }
    }

    /// <remarks/>
    public string celular
    {
        get
        {
            return this.celularField;
        }
        set
        {
            this.celularField = value;
        }
    }

    /// <remarks/>
    public string mensagem
    {
        get
        {
            return this.mensagemField;
        }
        set
        {
            this.mensagemField = value;
        }
    }
}

