﻿using Serilog;
using Serilog.Events;
using System;

namespace SMS
{
    public class Logger
    {
        private ILogger _logger;

        public Logger()
        {
            var threadId = Guid.NewGuid().ToString();

            var output = "[{Timestamp:dd-MM-yyyy HH:mm:ss}] [{Level}] [{idd}] {Message}{NewLine}{Exception}";

            _logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .Enrich.WithProperty("idd", threadId)
                .WriteTo.Console(outputTemplate: output)
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Debug)
                .WriteTo.Async(f => f.File(path: "Logs/log.txt",
                    rollingInterval: RollingInterval.Day,
                    outputTemplate: output, shared: true))
                .CreateLogger();
        }

        public ILogger Get()
        {
            return _logger;
        }
    }
}
