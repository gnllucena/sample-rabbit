﻿using Serilog;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SMS.Implementacao
{
    public class Servico
    {
        public void EnviarSms(string mensagemRabbit, ILogger log)
        {
            try
            {
                var objetoSms = DeserializarMensagem(mensagemRabbit);

                var servico = new TicketNotificationService.TicketNotificationClient();
                var mensagem = MontarObjetoSms(objetoSms);

#if !DEBUG
                servico.EnviarSms(mensagem);
#endif

                log.Information($"Mensagem enviada com sucesso, mensagem: [{mensagemRabbit}]");
            }
            catch (Exception ex)
            {
                log.Error($"Erro no envio da mensagem: {ex}");
                throw ex;
            }
        }

        private Model DeserializarMensagem(string mensagemRabbit)
        {
            try
            {
                string xml = mensagemRabbit;
                object obj;
                using (XmlReader reader = XmlReader.Create(new StringReader(xml)))
                {
                    obj = new XmlSerializer(typeof(Model)).Deserialize(reader);
                }

                return (Model)obj;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private TicketNotificationService.EnviarSms MontarObjetoSms(Model objetoServico)
        {
            var sms = new TicketNotificationService.EnviarSms();
            sms.identificador = objetoServico.identificador;
            sms.mensagem = objetoServico.mensagem;
            sms.celular = objetoServico.celular;

            return sms;
        }
    }
}
