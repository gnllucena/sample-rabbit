﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using Serilog;
using SMS.Implementacao;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SMS
{
    public class BuilderAmqp
    {
        private string _amqp;

        public BuilderAmqp(string usr, string pwd, string host, string port, string virtualHost)
        {
            //_amqp = string.Format("amqp://{0}:{1}@{2}:{3}/{4}", usr, pwd, host, port, virtualHost).Trim();
            _amqp = string.Format("amqp://{0}:{1}@{2}:{3}", usr, pwd, host, port ).Trim();
        }

        public string GetAmqp { get { return _amqp; } }
    }

    public class QueueWatcher
    {
        private ILogger _log;
        private Servico _servico;

        private readonly ConnectionFactory _queueConnFactory;
        
        public QueueWatcher(BuilderAmqp builder)
        {
            _log = new Logger().Get();

            _servico = new Servico();

            _queueConnFactory = new ConnectionFactory()
            {
                Uri = new Uri(builder.GetAmqp).ToString(),
                AutomaticRecoveryEnabled = true
            };
            
        }

        private IModel _model;
        private IConnection _queueConn;
        private string _consumerTag;
        private string _queue;

        public void Start(string queue, string exchange, string routingkey, CancellationToken ct)
        {
            ct.ThrowIfCancellationRequested();

            _queue = queue;

            _queueConn = _queueConnFactory.CreateConnection();

            _model = _queueConn.CreateModel();
            _model.BasicQos(0, 2, false);
            _model.QueueDeclare(queue, true, false, false, null);

            if (!string.IsNullOrWhiteSpace(exchange) && !string.IsNullOrWhiteSpace(routingkey))
                _model.QueueBind(queue, exchange, routingkey);

            var consumer = new EventingBasicConsumer(_model);

            consumer.Received += (ch, ea) =>
            {
                if (ct.IsCancellationRequested)
                    Stop();

                _log.Debug("Mensagem recebida na fila {0}.", _queue);

                string body = null;

                try
                {
                    body = Encoding.UTF8.GetString(ea.Body);

                    //
                    _servico.EnviarSms(body, _log);
                    //

                    _model.BasicAck(ea.DeliveryTag, true);
                }
                catch (AlreadyClosedException ex)
                {
                    _model.BasicNack(ea.DeliveryTag, true, true);
                    _log.Warning("Erro decorrente da parada do serviço. {0}", ex);
                }
                catch (Exception ex)
                {
                    _model.BasicNack(ea.DeliveryTag, true, true);
                    _log.Error("Erro inesperado data {0}. Mensagem: {1}", DateTime.Now, body);
                    _log.Fatal(ex.ToString());
                }
            };

            _consumerTag = _model.BasicConsume(queue, false, consumer);

            _log.Information("Consumo da fila {0} iniciado, consumer tag [{1}]", _queue, _consumerTag);
        }

        public void Stop()
        {
            _model.BasicCancel(_consumerTag);

            _queueConn.Close(0, string.Format("Serviço encerrado manualmente as {0}", DateTime.Now), 5000);

            _log.Warning("Consumo da fila {0} encerrado. Consumer tag '{1}'", _queue, _consumerTag);
        }
    }

    public class ConcurrentConsumers
    {
        private ILogger _log;

        private int _max;

        private IList<Task> _consumers { get; set; }

        private IList<CancellationTokenSource> _cancelations { get; set; }

        private int MaxConsumers()
        {
            var pConsumers = ConfigurationManager.AppSettings["RabbitMQ.consumers"];

            int consumers;
            int.TryParse(pConsumers, out consumers);

            consumers = consumers < 1 ? 1 : consumers;

            return consumers;
        }

        public ConcurrentConsumers(BuilderAmqp builder, string queue, string exchange, string routingkey)
        {
            _log = new Logger().Get();

            _max = MaxConsumers();

            _consumers = new List<Task>(_max);
            _cancelations = new List<CancellationTokenSource>(_max);

            for (int i = 0; i < _max; i++)
            {
                var watcher = new QueueWatcher(builder);

                var cts = new CancellationTokenSource();
                var ct = cts.Token;

                
                var task = new Task(() => watcher.Start(queue, exchange, routingkey, ct), ct);

                _cancelations.Add(cts);
                _consumers.Add(task);
            }
        }

        public void Start()
        {
            _log.Information($"Seviço iniciado com [{_consumers.Count}] consumidores.");

            foreach (var task in _consumers)
                task.Start();
        }

        public void Stop()
        {
            _log.Information("Seviço encerrado.");

            foreach (var token in _cancelations)
                token.Cancel();
        }
    }
}
