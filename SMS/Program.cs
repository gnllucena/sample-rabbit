﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace SMS
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string queueName = ConfigurationManager.AppSettings["RabbitMQ.queueName"];
                string exchange = ConfigurationManager.AppSettings["RabbitMQ.exchange"];
                string routingkey = ConfigurationManager.AppSettings["RabbitMQ.routingKey"];
                string port = ConfigurationManager.AppSettings["RabbitMQ.port"];
                string hostName = ConfigurationManager.AppSettings["RabbitMQ.hostName"];
                string userName = ConfigurationManager.AppSettings["RabbitMQ.userName"];
                string password = ConfigurationManager.AppSettings["RabbitMQ.password"];
                string quantidadeThreads = ConfigurationManager.AppSettings["RabbitMQ.quantidadeThreads"];
                string virtualHost = ConfigurationManager.AppSettings["RabbitMQ.virtualHost"];

                var builder = new BuilderAmqp(userName, password, hostName, port, virtualHost);

                //SetUpLog();
                HostFactory.Run(x =>
                {
                    x.Service<ConcurrentConsumers>(s =>
                    {
                        s.ConstructUsing(name => new ConcurrentConsumers(builder, queueName, exchange, routingkey));
                        s.WhenStarted(a => a.Start());
                        s.WhenStopped(a => a.Stop());
                    });

                    x.RunAsLocalSystem();

                    x.SetServiceName("Edenred.GAT.SMS");
                    x.SetDisplayName("Edenred.GAT.SMS");
                    x.SetDescription("GAT - Servico de Integração de SMS");
                });
            }
            catch (System.Exception e)
            {
                //_log.Fatal(e);
            }
        }
    }
}
